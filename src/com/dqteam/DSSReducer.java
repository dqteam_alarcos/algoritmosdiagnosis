package com.dqteam;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;



public class DSSReducer extends Reducer <Text, IntWritable, Text,Text>{
	private DoubleWritable result = new DoubleWritable();
		
	public void reduce(Text key, Iterable <IntWritable> values, Context context)throws IOException, InterruptedException{
		Configuration conf= context.getConfiguration();
		
		String separator = conf.get("SEPARATOR");
		String s = conf.get("SIZES");
		double nSymptoms = Double.parseDouble(s);
		//int [] indexes = conf.getInts("indexlist");
		//int sizelist = indexes.length;
		//String attributes = conf.get("nregistros");
		
		//double nSymptoms= 3;
		
		int sum = 0;
		for (IntWritable val : values) {
		 sum += val.get();
		}
		result.set(sum);
		double r =result.get();
		
		double porcentage = (r/nSymptoms)*100;
		double redondeo = Redondear(porcentage,0);
		
		context.write(new Text(key+";"), new Text(String.valueOf(redondeo)));
		}
		
	public static double Redondear(double numero,int digitos){
	      int cifras=(int) Math.pow(10,digitos);
	      return Math.rint(numero*cifras)/cifras;
	}
}

