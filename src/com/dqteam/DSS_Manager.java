package com.dqteam;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

	

	public class DSS_Manager extends Configured implements Tool {
		@Override
		public int run(String[] args) throws Exception {
			// TODO Auto-generated method stub
			Configuration conf = new Configuration();
			
		    conf.set("SEPARATOR", ";");
			conf.set("input", args[0]);
			conf.set("output", args[1]);
			int numberOfReducers = Integer.parseInt(args[2]);
			
			String sintomas=args[3];
			conf.set("SYMP",args[3]);
			
		    String[] sympss = sintomas.split(";");

		    int s = sympss.length;
		    //System.out.println(s+" "+sintomas);
		    String size = String.valueOf(s);
		    conf.set("SIZES", size);
		
			deleteOutputFileIfExists(args);
			
			Job job = Job.getInstance(conf);
			//Job job = new Job(conf);
			job.setJarByClass(DSS_Manager.class);
			job.setJobName("Manager");
			  
		    FileInputFormat.addInputPath(job, new Path(args[0]));
		    FileOutputFormat.setOutputPath(job, new Path(args[1]));
		    
		    
		    job.setNumReduceTasks(numberOfReducers);
		    job.setMapperClass(DSSMapper.class);
		    job.setReducerClass(DSSReducer.class);
		    //job.setInputFormatClass(TextInputFormat.class);
		    //job.setOutputValueClass(IntWritable.class);
		    //job.setOutputFormatClass(TextOutputFormat.class);
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(IntWritable.class);
		    
		   // job.setOutputKeyClass(Text.class);
			//ob.setOutputValueClass(Text.class);
			//job.setOutputKeyClass(Text.class);
			//job.setOutputValueClass(Text.class);
			
		
			System.exit(job.waitForCompletion(true)?0:1);
			return 0;
		}
		
		
		private void deleteOutputFileIfExists(String[] args) throws IOException {
			final Path output = new Path(args[1]);
			FileSystem.get(output.toUri(), getConf()).delete(output, true);
		}
		public static void main(String[] args) throws Exception {
			ToolRunner.run(new DSS_Manager(), args);
		}
		
	

}
