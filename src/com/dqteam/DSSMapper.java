package com.dqteam;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class DSSMapper extends Mapper <LongWritable, Text,Text,IntWritable> {
		
	Integer nAttributes;
	int suma;
	private final static IntWritable one = new IntWritable(1);
    private Text disease = new Text();
    
	public void map(LongWritable key, Text value, Context context)throws IOException, InterruptedException{
		Configuration conf= context.getConfiguration();
		String sintomas =  conf.get("SYMP");
		//String s = conf.get("SIZES");
		//int sizes = Integer.parseInt(s);
		//String [] symptoms ={"fiebre","tos","dolor de cabeza"};
		//String [] symptoms ={"fiebre","tos","dolor de cabeza"};
		String[] sympss = sintomas.split(";");
		
		//for (int i = 0; i<= sizes; i++){
			//symptoms[i]=sympss[i];
		//}
		
		
		String separator = conf.get("SEPARATOR");
		//int [] indexlist = conf.getInts("indexlist");
		//int sizelist = indexlist.length;
		//String attributes = conf.get("nregistros");
		
		//int sizeSymptoms = symptoms.length;
		//lectura del fichero en HDFS
		final String[] values = value.toString().split(separator);
		final String disease = values[0].toString();
		final String symptom = values[1].toString();
		
		if (isMatch(symptom,sympss)=="True"){
			context.write(new Text(disease), one);
		}
}
	
	public String isMatch (String data, String [] symptoms){
		String resultado = "False";
		boolean seguir=true;
		for (int i=0;i<symptoms.length && seguir;i++){
			if(data.equals(symptoms[i])){
				resultado="True";
				seguir=false;
			}
		}
		return resultado;
	}
}

